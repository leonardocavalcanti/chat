import { gql } from "apollo-server-express";

export default gql`
  scalar DateTime

  type Message {
    roomName: String!
    content: String!
    author: String
    created: DateTime 
  }

  extend type Query {
    Messages(roomName: String!): [Message]!
  }

  extend type Subscription {
    messageAdded(roomName: String!): Message
  }

  extend type Mutation {
      addMessage(roomName: String!, content: String!): Message @auth
      addMessageNoAuth(roomName: String!, content: String!, author: String): Message
  }
`