import { gql } from "apollo-server-express";

export default gql`
  type UserStatus {
    _id: String
    count: Boolean
    username: String
    isOnline: Boolean
  }

  extend type Query {
    Users(roomName: String!): [UserStatus]!
  }

  extend type Subscription {
    userStatus(roomName: String!, username: String!): UserStatus
  }
`