import { gql } from "apollo-server-express";
import userSessionSchema from "./userSession.schema";
import roomSchema from "./room.schema";
import messageSchema from "./message.schema";

const linkSchema = gql`
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
  type Subscription {
    _: Boolean
  }
`;

export default [linkSchema, userSessionSchema, roomSchema, messageSchema];