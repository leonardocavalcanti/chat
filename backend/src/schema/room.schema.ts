import { gql } from "apollo-server-express";

export default gql`
  type Room {
    _id: String,
    messages: Int
  }

  extend type Query {
    Rooms: [Room]!
  }
`