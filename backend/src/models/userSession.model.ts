import * as mongoose from 'mongoose';

export const userSessionModel = mongoose.model<mongoose.Document>('UserSession', new mongoose.Schema({
    username: String,
    roomName: String,
    isOnline: Boolean
}));