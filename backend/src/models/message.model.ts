import * as mongoose from 'mongoose';

export const messageModel = mongoose.model<mongoose.Document>('Message', new mongoose.Schema({
  roomName: String,
  content: String,
  author: String,
  created: Date
}));