import * as mongoose from 'mongoose';

export const roomModel = mongoose.model<mongoose.Document>('Room', new mongoose.Schema({
  name: String
}));