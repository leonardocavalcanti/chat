import { messageModel } from "../models/message.model";

export default {
    Query: {
        Rooms: () => messageModel.aggregate([
            {
                $group: {
                    _id: "$roomName",
                    messages: { $sum: 1 }
                }
            }
        ]).sort({ messages: -1 }).exec()
    }
}