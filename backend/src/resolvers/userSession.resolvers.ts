import { PubSub } from "apollo-server-express";
import { userSessionModel } from "../models/userSession.model";

const pubsub = new PubSub();

const withCancel = (asyncIterator, onCancel) => {
    const asyncReturn = asyncIterator.return;

    asyncIterator.return = () => {
        onCancel();
        return asyncReturn ? asyncReturn.call(asyncIterator) : Promise.resolve({ value: undefined, done: true });
    };

    return asyncIterator;
};

export default {
    Query: {
        Users: (_: any, { roomName }) => userSessionModel.aggregate([
            {
                $match: { roomName: roomName }
            },
            {
                $group: {
                    _id: "$username",
                    count: { $sum: 1 }
                }
            }
        ]).exec()
    },
    Subscription: {
        userStatus: {
            subscribe: async (_: any, { username, roomName }, _ctx, _info) => {
                if (!username) return;

                console.log(`start new subscription for ${username} on topic ${roomName}`)

                let userSession = await userSessionModel.exists({ username: username, roomName: roomName });

                if (!userSession) {
                    pubsub.publish(`${roomName}:user`, { userStatus: { username: username, isOnline: true } });
                }

                userSessionModel.create({ username: username, roomName: roomName, isOnline: true });

                return withCancel(pubsub.asyncIterator(`${roomName}:user`), async () => {
                    await userSessionModel.deleteOne({ username: username, roomName: roomName }).exec();

                    userSession = await userSessionModel.exists({ username: username, roomName: roomName });

                    if (!userSession) {
                        pubsub.publish(`${roomName}:user`, { userStatus: { username: username, isOnline: false } });
                    }

                    console.log(`ended subscription for ${username} on topic ${roomName}`);
                });
            }
        }
    }
}