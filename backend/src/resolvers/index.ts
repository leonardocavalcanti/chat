import messageResolvers from "./message.resolvers";
import roomResolvers from "./room.resolvers";
import userSessionResolvers from "./userSession.resolvers";

export default [messageResolvers, roomResolvers, userSessionResolvers]