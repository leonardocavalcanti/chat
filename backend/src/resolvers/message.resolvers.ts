import { PubSub } from "apollo-server-express";
import { messageModel } from "../models/message.model";
import { GraphQLDateTime } from "graphql-iso-date";

const pubsub = new PubSub();

export default {
    DateTime: GraphQLDateTime,

    Query: {
        Messages: (_: any, { roomName }) => {
            return messageModel.find({ roomName: roomName });
        }
    },
    Subscription: {
        messageAdded: {
            subscribe: (_: any, { roomName }: any, ctx: any, info: any) => pubsub.asyncIterator(roomName)
        }
    },
    Mutation: {
        addMessage: async (_: any, args, context) => {
            try {
                const user = context.kauth.accessToken.content;

                let response = await messageModel.create({ ...args, created: new Date(), author: user.name });

                pubsub.publish(args.roomName, { messageAdded: response })

                return response;
            } catch (e) {
                return e.message;
            }
        },
        addMessageNoAuth: async (_: any, args) => {
            try {
                let response = await messageModel.create({ ...args, created: new Date() });

                pubsub.publish(args.roomName, { messageAdded: response })

                return response;
            } catch (e) {
                return e.message;
            }
        }
    }
}