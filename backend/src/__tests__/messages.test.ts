import App from "../app"
import * as request from 'supertest'
import * as tokenRequester from "keycloak-request-token"
import { Server } from "http"
import * as ws from 'ws';
import ApolloClient from 'apollo-client'
import { execute } from 'apollo-link';
import { WebSocketLink } from "apollo-link-ws";
import { SubscriptionClient } from "subscriptions-transport-ws";
import { gql } from "apollo-server-express"

const GRAPHQL_ENDPOINT = 'ws://localhost:0/graphql'

const ADD_MESSAGE = {
    operationName: "addMessage",
    variables: { "roomName": "test-room", content: "test content" },
    query: `mutation addMessage($roomName: String!, $content: String!) 
    {
        addMessage(roomName: $roomName, content: $content) { 
            created 
        }
    }`
};

//TODO Solve authentication bug
const ADD_MESSAGE_NO_AUTH = {
    operationName: "addMessageNoAuth",
    variables: { "roomName": "test-room", content: "test content", author: "Test User" },
    query: `mutation addMessageNoAuth($roomName: String!, $content: String!, $author: String!) 
    {
        addMessageNoAuth(roomName: $roomName, content: $content, author: $author) { 
            created 
        }
    }`
};

const QUERY_MESSAGES = {
    query:
        `{
            Messages(roomName: "test") {
                roomName,
                content,
                author,
                created
            }
        }`
}

const baseUrl = 'http://localhost:8080/auth';

const settings = {
    username: 'user',
    password: 'password',
    grant_type: 'password',
    client_id: 'chat',
    realmName: 'chat'
};

const wsClient = new SubscriptionClient("ws://localhost:0/graphql", { reconnect: true }, ws);

const link = new WebSocketLink(wsClient);

async function getToken() {
    return await tokenRequester(baseUrl, settings)
}

interface options {
    networkinterface: any
}

describe('Messages GraphQL', () => {
    let server: Server;
    let apollo: ApolloClient<any>;

    beforeAll(() => {
        process.env.MONGO_URI = (global as any).MONGO_URI;
        process.env.PORT = "0";
        process.env.KEYCLOAK_REALM = "chat"
        process.env.KEYCLOAK_URL = "http://localhost:8080/auth"
        process.env.KEYCLOAK_CLIENT_ID = "chat"

        server = new App().listen();
    });

    afterAll(() => {
        server.close();
    });

    it('should fail to add message when user is not authenticated', done => {
        request(server)
            .post('/graphql')
            .send(ADD_MESSAGE)
            .expect(200)
            .end((err, res) => {
                if (err) { return done(err); }
                expect(res.text).toContain("User not Authenticated");
                done();
            });
    })

    //TODO Solve authentication bug
    it('should add message when user is authenticated', async done => {
        let token = await getToken();

        request(server)
            .post('/graphql')
            .set({
                'Authorization': `Bearer ${token}`
            })
            .send(ADD_MESSAGE_NO_AUTH)
            .expect(200)
            .end((err, res) => {
                if (err) { return done(err); }
                done();
            });
    })

    it('should return messages', done => {
        request(server)
            .post('/graphql')
            .send(QUERY_MESSAGES)
            .expect(200)
            .end((err, res) => {
                if (err) { return done(err); }
                expect(JSON.parse(res.text)).toStrictEqual({ data: { Messages: [] } });
                done();
            });
    })

    //TODO Figure out why subscription is not receiving notifications
    it('should subscribe, add a message and get notified', async done => {
        const subscriptionPromise = new Promise((resolve, reject) => {
            execute(link, {
                query: gql`
                subscription messageAdded($roomName: String!) {
                  messageAdded(roomName: $roomName) {
                      content,
                      author,
                      created
                  }
                }`
            }).subscribe({
                next: (data) => {
                    console.log(data);
                    resolve();
                },
                error: (err) => {
                    console.log(err);
                    reject();
                }
            })
        });

        subscriptionPromise.then(res => {
            console.log(res);
            done();
        })

        request(server)
            .post('/graphql')
            .send(ADD_MESSAGE_NO_AUTH)
            .expect(200).end((err, res) => {
                console.log(res.body)
                done()
            });
    })
})