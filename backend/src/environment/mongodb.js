const NodeEnvironment = require('jest-environment-node');
const MongodbMemoryServer = require('mongodb-memory-server');

class MongoDbEnvironment extends NodeEnvironment {
    constructor(config) {
        super(config);
        this.mongod = new MongodbMemoryServer.default({
            instance: {
            },
            binary: {
                version: '3.6.1',
            },
            // debug: true,
        });
    }

    async setup() {
        await super.setup();
        
        console.log('\n# MongoDB Environment Setup #');

        this.global.MONGO_URI = await this.mongod.getConnectionString();
    }

    async teardown() {
        console.log('\n# MongoDB Environment Teardown #');
        await super.teardown();
        await this.mongod.stop();
    }

    runScript(script) {
        return super.runScript(script);
    }
}

module.exports = MongoDbEnvironment;