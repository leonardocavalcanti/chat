import * as Keycloak from 'keycloak-connect';
import * as expressSession from 'express-session';

const memoryStore = new expressSession.MemoryStore();

const {
    KEYCLOAK_REALM,
    KEYCLOAK_URL,
    KEYCLOAK_CLIENT_ID
} = process.env;

export const keycloak = new Keycloak({ store: memoryStore }, {realm: KEYCLOAK_REALM, serverUrl: KEYCLOAK_URL, clientId: KEYCLOAK_CLIENT_ID });

export const session = expressSession({
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
});