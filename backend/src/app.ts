import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import * as cors from 'cors';
import { session, keycloak } from './middleware/auth.middleware';
import { ApolloServer, gql } from 'apollo-server-express';
import { KeycloakContext, KeycloakTypeDefs, KeycloakSchemaDirectives, KeycloakSubscriptionHandler, KeycloakSubscriptionContext } from 'keycloak-connect-graphql';
import { createServer, Server } from "http";
import { userSessionModel } from './models/userSession.model';
import schema from './schema';
import resolvers from './resolvers'

class App {
    public app: express.Application;
    private httpServer: Server;

    constructor() {
        this.app = express();

        this.connectToTheDatabase();
        this.initializeMiddlewares();
    }

    public listen() {
        this.httpServer.listen(process.env.PORT, () => {
            console.log(`App listening on the port ${process.env.PORT}`);
        });

        return this.httpServer;
    }

    public close() {
        this.httpServer.close();
    }

    private initializeMiddlewares() {
        this.app.use(bodyParser.json());
        this.app.use(session);
        this.app.use(cors({
            origin: ['http://localhost:3000', 'htpp://localhost'],
            credentials: true
        }));
        this.app.use('/graphql', keycloak.middleware());

        const keycloakSubscriptionHandler = new KeycloakSubscriptionHandler({ keycloak: keycloak as any, protect: false })

        const server = new ApolloServer({
            typeDefs: [gql`${KeycloakTypeDefs}`, ...schema], // 1. Add the Keycloak Type Defs
            schemaDirectives: KeycloakSchemaDirectives, // 2. Add the KeycloakSchemaDirectives
            resolvers,
            subscriptions: {
                onConnect: async (connectionParams: any, websocket, connectionContext) => {
                    const token = await keycloakSubscriptionHandler.onSubscriptionConnect(connectionParams, websocket, connectionContext)
                    return {
                        kauth: new KeycloakSubscriptionContext(token as any)
                    }
                }
            },
            context: ({ req }) => {
                return {
                    kauth: new KeycloakContext({ req: req as any }) // 3. add the KeycloakContext to `kauth`
                }
            }
        });

        server.applyMiddleware({ app: this.app })
        this.httpServer = createServer(this.app)
        server.installSubscriptionHandlers(this.httpServer)
    }

    private connectToTheDatabase() {
        const {
            MONGO_URI
        } = process.env;

        mongoose.connect(MONGO_URI);

        userSessionModel.deleteMany({}).exec();
    }
}

export default App;