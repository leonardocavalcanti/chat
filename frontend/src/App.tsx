import React from 'react';
import Keycloak, { KeycloakConfig } from 'keycloak-js';
import { KeycloakProvider } from '@react-keycloak/web';

import { AppRouter } from './routes'

const options: KeycloakConfig = {
  url: process.env.REACT_APP_KEYCLOAK_URL,
  realm: process.env.REACT_APP_KEYCLOAK_REALM || "",
  clientId: process.env.REACT_APP_KEYCLOAK_CLIENT_ID || ""
}

const keycloak = Keycloak(options);

const App = () => {
  return (
    <KeycloakProvider keycloak={keycloak}>
      <AppRouter />
    </KeycloakProvider>
  );
}

export default App;
