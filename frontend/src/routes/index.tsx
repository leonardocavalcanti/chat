import React from 'react'
import { Redirect, Route, BrowserRouter } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'

import RoomsComponent from '../components/Rooms/RoomsComponent'
import RoomComponent from '../components/Room/RoomComponent'

import { useApolloClient } from "@apollo/react-hooks";

export const AppRouter = () => {
    const [keycloak, initialized] = useKeycloak();

    const client = useApolloClient();

    if (!initialized) {
        return <div>Loading...</div>
    }

    if (keycloak.authenticated) {
        localStorage.setItem("token", keycloak.token as string)

        client.writeData({ data: { userinfo: { __typename: 'userinfo', username: (keycloak.tokenParsed as any).name, authenticated: true } } })
    } else {
        localStorage.removeItem("token");
    }

    return (
        <BrowserRouter>
            <Route exact path="/" render={() => <Redirect to="/rooms" />} />
            <Route exact path="/rooms" component={RoomsComponent} />
            <Route path="/rooms/:roomName" component={RoomComponent} />
        </BrowserRouter>
    )
}