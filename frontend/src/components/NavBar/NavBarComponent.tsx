import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useKeycloak } from '@react-keycloak/web';
import { useParams, useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
        },
        title: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('sm')]: {
                display: 'none',
            },
        }
    }),
);

interface NavBarComponentProps {
    handleDrawerToggle?: any;
}

export default function NavBarComponent(props: NavBarComponentProps) {
    const { handleDrawerToggle } = props;

    const { data } = useQuery(gql`
        {
            userinfo @client {
                username,
                authenticated
            }
        }
    `);
    
    const { username, authenticated } = data.userinfo;

    const [keycloak] = useKeycloak();
    
    const { roomName } = useParams();
    
    const classes = useStyles();
    
    const history = useHistory();
    
    function quitRoom() {
        history.push("/rooms");
    }

    return (
        <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
                {handleDrawerToggle &&
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </IconButton>}

                {!roomName
                    ? <Typography variant="h6" className={classes.title}>Wellcome to the Chat, {authenticated ? username : 'Guest'}!</Typography>
                    : <><Typography variant="h6" className={classes.title}>{roomName}</Typography><Button color="inherit" onClick={quitRoom}>Quit Room</Button></>}

                {authenticated
                    ? <Button color="inherit" onClick={() => keycloak.logout()}>Logout</Button>
                    : <><Button color="inherit" onClick={() => keycloak.login()}>Login</Button><Button color="inherit" onClick={() => keycloak.register()}>Register</Button></>}
            </Toolbar>
        </AppBar>
    );
}