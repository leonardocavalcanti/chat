import React, { useState, useRef, useEffect } from 'react'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

import { gql } from 'apollo-boost';
import { useSubscription, useQuery, useMutation } from '@apollo/react-hooks';
import { useParams } from 'react-router-dom';
import { AppBar, Toolbar, FormControl, Input, InputAdornment, IconButton, Icon, Snackbar } from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import RoomUsersComponent from '../RoomUsers/RoomUsersComponent';
import NavBarComponent from '../NavBar/NavBarComponent';

export const MESSAGES_SUBSCRIPTION = gql`
  subscription messageAdded($roomName: String!) {
    messageAdded(roomName: $roomName) {
        content,
        author,
        created
    }
  }
`;

export const LIST_MESSAGES = gql`
    query Messages($roomName: String!){
        Messages(roomName: $roomName) {
            content,
            author,
            created
        }
    }
`;

export const ADD_MESSAGE = gql`
  mutation addMessage($roomName: String!, $content: String!) {
    addMessage(roomName: $roomName, content: $content) {
      created
    }
  }
`;

export const USER_INFO_QUERY = gql`
    query userinfo {
        userinfo @client {
            username,
            authenticated
        }
    }
`;

let messagesInitial: any[] = [];

const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            paddingBottom: 64
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.paper
        },
        inline: {
            display: 'inline',
        },
        appBar: {
            top: 'auto',
            bottom: 0,
            [theme.breakpoints.up('sm')]: {
                width: `calc(100% - ${drawerWidth}px)`,
                marginLeft: drawerWidth,
            },
        },
        margin: {
            margin: theme.spacing(1),
        },
        form: {
            width: '100%'
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
        toolbar: theme.mixins.toolbar
    }),
);

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default () => {
    const { roomName } = useParams();

    const [messages, setMessages] = useState(messagesInitial);
    const [showMessage, setShowMessage] = useState(false);
    const [message, setMessage] = useState("");
    const [messageContent, setMessageContent] = useState("");
    const [firstScroll, setFirstScroll] = useState(true);

    const messagesContainerRef = useRef<HTMLDivElement>(null);

    const { data } = useQuery(USER_INFO_QUERY);

    const { username, authenticated } = data.userinfo;

    const classes = useStyles();

    const [addMessage] = useMutation(ADD_MESSAGE, {
        onError: (error) => {
            setShowMessage(true);
            setMessage(`Error while trying to send message: ${error.message}`);
        }
    });

    useQuery(LIST_MESSAGES, {
        variables: { roomName }, onCompleted: (data) => {
            setMessages([...messages, ...data.Messages]);
        }
    });

    useSubscription(MESSAGES_SUBSCRIPTION, {
        variables: { roomName }, onSubscriptionData: (data) => {
            if (firstScroll)
                setFirstScroll(false);
            setMessages([...messages, data.subscriptionData.data.messageAdded]);
        }
    });

    useEffect(() => {
        messagesContainerRef.current?.scrollIntoView({ behavior: firstScroll ? "auto" : "smooth" });
    }, [messages, firstScroll]);

    function sendMessage(event: any) {
        if (messageContent && messageContent.length > 0) {
            addMessage({ variables: { roomName: roomName, content: messageContent } });
    
            setMessageContent("");
        }

        event.preventDefault();
    }

    const [mobileOpen, setMobileOpen] = useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const handleCloseMessage = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setShowMessage(false);
    };

    return (
        <div className={classes.root}>
            <Snackbar open={showMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
                <Alert onClose={handleCloseMessage} severity="error">
                    {message}
                </Alert>
            </Snackbar>
            <NavBarComponent handleDrawerToggle={handleDrawerToggle} />
            <RoomUsersComponent mobileOpen={mobileOpen} handleDrawerToggle={handleDrawerToggle} />
            <main className={classes.content}>
                <div className={classes.toolbar} />
                <List className={classes.list}>
                    {messages && messages.map(({ content, created, author }: { content: string, created: Date, author: string }, index: number) => (
                        (author !== username ?
                            <ListItem alignItems="flex-start" key={index}>
                                <ListItemAvatar>
                                    <Avatar><Icon>person</Icon></Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={author}
                                    secondary={
                                        <React.Fragment>
                                            {content}
                                        </React.Fragment>
                                    }
                                />
                            </ListItem>
                            :
                            <ListItem alignItems="flex-start" key={index}>
                                <ListItemText
                                    style={{ textAlign: 'right', marginRight: 20 }}
                                    primary={author}
                                    secondary={
                                        <React.Fragment>
                                            {content}
                                        </React.Fragment>
                                    }
                                />
                                <ListItemAvatar>
                                    <Avatar><Icon>person</Icon></Avatar>
                                </ListItemAvatar>
                            </ListItem>)
                    ))}
                </List>
                <div style={{ float: "left", clear: "both" }} ref={messagesContainerRef}></div>
            </main>
            {authenticated && <AppBar position="fixed" color="secondary" className={classes.appBar}>
                <Toolbar>
                    <form className={classes.form} onSubmit={sendMessage}>
                        <FormControl fullWidth className={classes.margin}>
                            <Input
                                type="text"
                                placeholder="Type a message..."
                                value={messageContent}
                                onChange={(e) => setMessageContent(e.target.value)}
                                fullWidth
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            type="submit"
                                        >
                                            <Icon>send</Icon>
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </form>
                </Toolbar>
            </AppBar>
            }
        </div>
    );
}