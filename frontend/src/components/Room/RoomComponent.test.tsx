import React from 'react';
import { MockedProvider } from '@apollo/react-testing';
import { render } from '@testing-library/react';
import RoomComponent, { LIST_MESSAGES, USER_INFO_QUERY } from './RoomComponent';
import { BrowserRouter } from 'react-router-dom';

const mocks = [
  {
    request: {
      query: LIST_MESSAGES
    },
    result: {
      data: {
        Messages: [{ content: 'test', author: 'user', created: new Date() }],
      },
    },
  },
  {
    request: {
      query: USER_INFO_QUERY
    },
    result: {
      data: {
        userinfo: { username: 'test', authenticated: true },
      },
    },
  },
];

it('renders', () => {
  render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <BrowserRouter>
        <RoomComponent />
      </BrowserRouter>
    </MockedProvider>,
  );
});
