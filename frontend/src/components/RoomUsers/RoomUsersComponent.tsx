import React, { useState, useEffect } from 'react';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import { ListSubheader, Icon, Snackbar } from '@material-ui/core';
import { gql } from 'apollo-boost';
import { useSubscription, useQuery } from '@apollo/react-hooks';
import { useParams } from 'react-router-dom';

const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        drawer: {
            [theme.breakpoints.up('sm')]: {
                width: drawerWidth,
                flexShrink: 0,
            },
        },
        appBar: {
            [theme.breakpoints.up('sm')]: {
                width: `calc(100% - ${drawerWidth}px)`,
                marginLeft: drawerWidth,
            },
        },
        menuButton: {
            marginRight: theme.spacing(2),
            [theme.breakpoints.up('sm')]: {
                display: 'none',
            },
        },
        toolbar: theme.mixins.toolbar,
        drawerPaper: {
            width: drawerWidth,
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
    }),
);

const USERS_SUBSCRIPTION = gql`
  subscription userStatus($roomName: String!, $username: String!) {
    userStatus(roomName: $roomName, username: $username) {
        username,
        isOnline
    }
  }
`;

const LIST_USERS = gql`
    query Users($roomName: String!){
        Users(roomName: $roomName) {
            _id
        }
    }
`;

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

interface RoomUsersProps {
    mobileOpen?: any;
    handleDrawerToggle?: any;
}

export default function RoomUsersComponent(props: RoomUsersProps) {
    const { data } = useQuery(gql`
        {
            userinfo @client {
                username,
                authenticated
            }
        }
    `);

    const { username, authenticated } = data.userinfo;

    const { mobileOpen, handleDrawerToggle } = props;
    const { roomName } = useParams();
    const [showMessage, setShowMessage] = useState(false);
    const [message, setMessage] = useState("");
    const [users, setUsers] = useState<any[]>([{ username: username, isOnline: true }]);

    const classes = useStyles();
    const theme = useTheme();

    useEffect(() => {
        if (!authenticated) {
            setShowMessage(true);
            setMessage(`You joined the room as a guest (${username}), login or register to start chatting!`);
        }
    }, [setShowMessage, setMessage, username, authenticated])

    useSubscription(USERS_SUBSCRIPTION, {
        variables: { roomName, username: username },
        onSubscriptionData: (data) => {
            let userStatus = data.subscriptionData.data.userStatus;

            if (!userStatus.isOnline) {
                let user = users.find(user => user.username === userStatus.username);

                users.splice(users.indexOf(user), 1);

                setUsers([...users]);

                setShowMessage(true);
                setMessage(`${userStatus.username} left the room! :(`);
            } else {
                let user = users.find(user => user.username === userStatus.username);
                if (!user) {
                    setUsers([...users, userStatus]);

                    setShowMessage(true);
                    setMessage(`${userStatus.username} joined the room! :)`)
                }
            }
        }
    });

    useQuery(LIST_USERS, {
        variables: { roomName }, onCompleted: (data: any) => {
            let u: any[] = [];

            data.Users.forEach((user: any) => {
                if (!users.find(u => u.username === user.username)) {
                    u.push({username: user._id, isOnline: true});
                }
            });

            setUsers([...users, ...u]);
        }
    });

    const drawer = (
        <div>
            <div className={classes.toolbar} />
            <List
                subheader={
                    <ListSubheader component="div">Online Users</ListSubheader>
                }>
                {users.map((user, index) => (
                    <ListItem key={index}>
                        <ListItemIcon><Icon>person</Icon></ListItemIcon>
                        <ListItemText primary={user.username === username ? `(Me) ${user.username}` : user.username} />
                    </ListItem>
                ))}
            </List>
        </div>
    );

    const handleCloseMessage = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setShowMessage(false);
    };

    return (
        <nav className={classes.drawer} aria-label="mailbox folders">
            <Snackbar open={showMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
                <Alert onClose={handleCloseMessage} severity="info">
                    {message}
                </Alert>
            </Snackbar>
            <Hidden smUp implementation="css">
                <Drawer
                    variant="temporary"
                    anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                >
                    {drawer}
                </Drawer>
            </Hidden>
            <Hidden xsDown implementation="css">
                <Drawer
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    variant="permanent"
                    open
                >
                    {drawer}
                </Drawer>
            </Hidden>
        </nav>
    );
}