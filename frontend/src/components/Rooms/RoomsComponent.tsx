import React from 'react'

import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';
import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from "@material-ui/core/Icon";
import { ListSubheader } from '@material-ui/core';
import NavBarComponent from '../NavBar/NavBarComponent';

const LIST_ROOMS = gql`
    {
        Rooms {
            _id,
            messages
        }
    }
`;

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    toolbar: theme.mixins.toolbar,
}));

export default () => {
    const classes = useStyles();
    const { loading, error, data } = useQuery(LIST_ROOMS);
    
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <div className={classes.root}>
            <NavBarComponent />
            <div className={classes.toolbar} />
            <List component="nav"
                subheader={
                    <ListSubheader component="div">Most active rooms</ListSubheader>
                }>
                {data.Rooms.map((room: any, index: number) => (
                    <ListItem key={index} button component={Link} to={`/rooms/${room._id}`}>
                        <ListItemIcon><Icon>meeting_room</Icon></ListItemIcon>
                        <ListItemText primary={room._id} secondary={`${room.messages} message(s)`} />
                    </ListItem>
                ))}
            </List>
        </div>
    )
}